package com.insightfullogic.java8.answers.chapter6;

import org.openjdk.jmh.annotations.GenerateMicroBenchmark;
import org.openjdk.jmh.annotations.Setup;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * Created by xiaohyb on 2015/11/27.
 */
public class Test6 {

    @Test
    public void test_ParallelSetAll() {
        long[] longs = new long[100];
        Arrays.parallelSetAll(longs, i -> i + 1);
        System.out.println();
    }

    @Test
    public void test_move() {
        double[] doubles = {0, 1, 2, 3, 4, 3.5};
        int n = 3;
        double[] sums = Arrays.copyOf(doubles, doubles.length);
        Arrays.parallelPrefix(sums, Double::sum);
        int start = n - 1;
        double[] result = IntStream.range(start, sums.length)
                .mapToDouble(i -> {
                    double prefix = i == start ? 0 : sums[i - n];
                    return (sums[i] - prefix) / n;
                }).toArray();
    }

    public static int serialThrough(List<Integer> numbers) {
        return numbers.stream()
                .reduce(5, (acc, x) -> x * acc);
    }

    public static int parralelThrough(List<Integer> numbers) {
        return 5* numbers.parallelStream()
                .reduce(1, (acc, x) -> x * acc);
    }

    @Test
    public void testMultiple() {
        List<Integer> numbers = new ArrayList<Integer>(){{
            add(2);
            add(3);
            add(4);
            add(5);
        }};

        System.out.println(Test6.serialThrough(numbers));
        System.out.println(Test6.parralelThrough(numbers));
    }

    private List<Long> arrayListOfNumbers;
    private List<Long> linkedListOfNumbers;

    @Test
    public void testSpeed() {
        init();
        {
            long t1 = System.currentTimeMillis();
            System.out.println(serialSlowSumOfSquares());
            long t2 = System.currentTimeMillis();
            System.out.println("serialSlowSumOfSquares: " + (t2 - t1));
        }

        {
            long t1 = System.currentTimeMillis();
            System.out.println(serialIntermediateSumOfSquares());
            long t2 = System.currentTimeMillis();
            System.out.println("serialIntermediateSumOfSquares: " + (t2 - t1));
        }

        {
            long t1 = System.currentTimeMillis();
            System.out.println(serialFastSumOfSquares());
            long t2 = System.currentTimeMillis();
            System.out.println("serialFastSumOfSquares: " + (t2 - t1));
        }

        {
            long t1 = System.currentTimeMillis();
            System.out.println(slowSumOfSquares());
            long t2 = System.currentTimeMillis();
            System.out.println("slowSumOfSquares: " + (t2 - t1));
        }

        {
            long t1 = System.currentTimeMillis();
            System.out.println(intermediateSumOfSquares());
            long t2 = System.currentTimeMillis();
            System.out.println("intermediateSumOfSquares: " + (t2 - t1));
        }

        {
            long t1 = System.currentTimeMillis();
            System.out.println(fastSumOfSquares());
            long t2 = System.currentTimeMillis();
            System.out.println("fastSumOfSquares: " + (t2 - t1));
        }
    }

    private void init() {
        arrayListOfNumbers= new ArrayList<>();
        addNumbers(arrayListOfNumbers);

        linkedListOfNumbers = new LinkedList<>();
        addNumbers(linkedListOfNumbers);
    }

    private void addNumbers(List<Long> container) {
        LongStream.range(0, 15_000_000)
                .forEach(container::add);
    }

    private long serialSlowSumOfSquares() {
        return linkedListOfNumbers.stream()
                .map(x -> x * x)
                .reduce(0L, (acc, x) -> acc + x);
    }

    private long serialIntermediateSumOfSquares() {
        return arrayListOfNumbers.stream()
                .map(x -> x * x)
                .reduce(0L, (acc, x) -> acc + x);
    }

    public long serialFastSumOfSquares() {
        return arrayListOfNumbers.stream()
                .mapToLong(x -> x * x)
                .sum();
    }

    private long slowSumOfSquares() {
        return linkedListOfNumbers.parallelStream()
                .map(x -> x * x)
                .reduce(0L, (acc, x) -> acc + x);
    }

    private long intermediateSumOfSquares() {
        return arrayListOfNumbers.parallelStream()
                .map(x -> x * x)
                .reduce(0L, (acc, x) -> acc + x);
    }

    public long fastSumOfSquares() {
        return arrayListOfNumbers.parallelStream()
                .mapToLong(x -> x * x)
                .sum();
    }


}
