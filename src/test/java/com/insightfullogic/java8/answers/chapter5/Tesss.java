package com.insightfullogic.java8.answers.chapter5;

import com.insightfullogic.java8.examples.chapter4.Data;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Created by xiaohyb on 2015/11/16.
 */
public class Tesss {

    @Test
    public void test_00() {
        Set<Integer> numbers = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List<Integer> sameOrder = numbers.stream().collect(Collectors.toList());
        assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9), sameOrder);
    }

    @Test
    public void test_01() {
        Set<Integer> numbers = new HashSet<>(Arrays.asList(4, 2, 3, 1));

        numbers.stream()
                .sorted()
                .forEach(System.out::println);

        System.out.println();
        numbers.stream().forEachOrdered(System.out::println);
    }

    @Test
    public List<Data> getTelephoneData(List<String> telephones, List<Data> dataList) {
        return dataList.stream()
                .filter(data -> telephones.contains(data.mobile))
                .collect(Collectors.<Data>toList());
    }

    @Test
    public Set<String> getTelephoneDatas(List<String> telephones, List<Data> dataList) {
        return
                dataList.stream()
                .filter(data -> telephones.contains(data.mobile))
                .map(data1 -> data1.mobile)
                .collect(Collectors.<String>toSet());
    }

    @Test
    public void ll() {
        StringBuilder builder = new StringBuilder("123");
        builder.append("4");

        StringBuilder builder1 = new StringBuilder("ab");
        builder.append(builder1, 0, builder1.length()-1);
        System.out.println(builder.toString());
    }

    @Test
    public void test_s() {
        System.out.println(6 & 8 );
    }

}
