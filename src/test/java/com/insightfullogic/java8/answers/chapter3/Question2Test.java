package com.insightfullogic.java8.answers.chapter3;

import com.insightfullogic.java8.examples.chapter1.SampleData;
import org.junit.Test;

import java.util.Arrays;

import static com.insightfullogic.java8.answers.chapter3.Question2.countBandMembersInternal;
import static org.junit.Assert.assertEquals;

public class Question2Test {

    @Test
    public void internal() {
        int c = countBandMembersInternal(Arrays.asList(SampleData.johnColtrane, SampleData.theBeatles));
        assertEquals(4, c);
    }

}
