package com.insightfullogic.java8.examples.chapter4;

import com.insightfullogic.java8.examples.chapter1.SampleData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PrimitivesTest {

    @Test
    public void albumStatistics() {
        Primitives.printTrackLengthStatistics(SampleData.aLoveSupreme);
    }

    @Test
    public void testContains() {
        List<String> mobiles = Stream.of("1234", "1111", "2222", "4321").collect(Collectors.toList());

        Data data_1 = new Data("Henry", "1234");
        Data data_2 = new Data("Lily", "4321");
        Data data_3 = new Data("Taylor", "0000");
        Data data_4 = new Data("Julius", "1111");
        Data data_5 = new Data("Buck", "7777");
        List<Data> dataList = Stream.of(data_1, data_2, data_3, data_4, data_5).collect(Collectors.toList());
        List<Data> remain = getSubList(mobiles, dataList);
        System.out.println(remain.get(0));
    }

    private List<Data> getSubList(List<String> mobiles, List<Data> dataList) {
        return dataList.stream()
                .filter(data -> mobiles.contains(data.mobile))
                .collect(Collectors.toList());
    }
}
