package com.insightfullogic.java8.examples.chapter4;

/**
 * Created by HenryAXE on 2015/11/18.
 */
public class Data {

    public String name;

    public String mobile;

    public Data() {
    }

    public Data(String name, String mobile) {
        this.name = name;
        this.mobile = mobile;
    }
}
