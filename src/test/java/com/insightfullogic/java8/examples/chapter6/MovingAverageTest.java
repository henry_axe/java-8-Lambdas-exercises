package com.insightfullogic.java8.examples.chapter6;

import com.insightfullogic.java8.examples.chapter1.Artist;
import com.insightfullogic.java8.exercises.chapter5.LongestName;
import com.insightfullogic.java8.exercises.chapter5.WordCount;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.IntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;
import static org.junit.Assert.assertArrayEquals;

public class MovingAverageTest {

    /*@Test(expected = IllegalArgumentException.class)
    public void emptyArray() {
        ArrayExamples.simpleMovingAverage(new double[]{}, 3);
    }*/

    @Test
    public void smallArray() {
        double[] input = {0, 1, 2, 3, 4, 3.5};
        double[] result = ArrayExamples.simpleMovingAverage(input, 3);
        System.out.println(Arrays.toString(result));
        double[] expected = {1, 2, 3, 3.5};
        assertArrayEquals(expected, result, 0.0);
    }

    @Test
    public void testDiceRolls() {
        long begin = System.currentTimeMillis();
        new DiceRolls().parallelDiceRolls();
        long end = System.currentTimeMillis();
        System.out.println(end - begin);
    }

    @Test
    public void getNameLengthMax() {
        Stream<Artist> artistStream = Stream.of(new Artist("王菲", "CN"),
                new Artist("Taylor Swift", "USA"), new Artist("Celine Dion", "CA"));
        Artist artist = LongestName.byCollecting(artistStream);
        System.out.println(artist.getName());
    }

    @Test
    public void wordCount() {
        Stream<String> names = Stream.of("John", "Paul", "George", "John", "Paul", "John");
        Map<String, Long> map = WordCount.countWords(names);
        System.out.println();
    }

    @Test
    public void sequentialDiceRolls() {
        int N = 1000000000;
        double fraction = 1L / N;
        long t1 = System.currentTimeMillis();
        Map<Integer, Long> listMap = IntStream.range(0, N)
                .mapToObj(getD())
                .collect(groupingBy(key -> key, summingLong(value -> value)));
        long t2 = System.currentTimeMillis();
        System.out.println(t2 - t1);
    }

    @Test
    public void parallelDiceRolls() {
        int N = 1000000000;
        double fraction = 1L / N;
        long t1 = System.currentTimeMillis();
        Map<Integer, Long> listMap = IntStream.range(0, N)
                .parallel()
                .mapToObj(getD())
                .collect(groupingBy(key -> key, summingLong(value -> value)));
        long t2 = System.currentTimeMillis();
        System.out.println(t2 - t1);
    }

    private static IntFunction<Integer> getD() {
        return i -> ThreadLocalRandom.current().nextInt(1, 7);
    }

    private static IntFunction<Integer> twoDiceThrows() {
        return i -> {
            ThreadLocalRandom random = ThreadLocalRandom.current();
            int firstThrow = random.nextInt(1, 7);
            int secondThrow = random.nextInt(1, 7);
            return firstThrow + secondThrow;
        };
    }
}
