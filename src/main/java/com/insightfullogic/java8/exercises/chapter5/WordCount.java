package com.insightfullogic.java8.exercises.chapter5;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class WordCount {

    public static Map<String, Long> countWords(Stream<String> names) {
//        return Exercises.replaceThisWithSolution();
        return names.collect(groupingBy(name -> name, counting()));
    }


}
