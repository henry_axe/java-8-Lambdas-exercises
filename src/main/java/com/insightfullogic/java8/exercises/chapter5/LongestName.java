package com.insightfullogic.java8.exercises.chapter5;

import com.insightfullogic.java8.examples.chapter1.Artist;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LongestName {

    private static Comparator<Artist> byNameLength = Comparator.comparing(artist -> artist.getName().length());

    public static Artist byReduce(Stream<Artist> artists) {
//        return Exercises.replaceThisWithSolution();
        return artists.reduce((acc, ele) -> byNameLength.compare(acc, ele) >= 0 ? acc : ele)
                .orElseThrow(RuntimeException::new);
    }

    public static Artist byCollecting(Stream<Artist> artists) {
//        return Exercises.replaceThisWithSolution();
//        return artists.max(byNameLength)
//                .get();
        return artists.collect(Collectors.maxBy(byNameLength))
                .orElseThrow(RuntimeException::new);
    }

}
