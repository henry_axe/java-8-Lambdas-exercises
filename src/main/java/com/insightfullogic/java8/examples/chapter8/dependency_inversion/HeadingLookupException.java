package com.insightfullogic.java8.examples.chapter8.dependency_inversion;

/**
 * Created by xiaohyb on 2015/12/8.
 */
public class HeadingLookupException extends RuntimeException {

    public HeadingLookupException() {
    }

    public HeadingLookupException(String message) {
        super(message);
    }

    public HeadingLookupException(Exception e) {
        super(e.getMessage());
    }
}
